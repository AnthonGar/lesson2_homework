#include"Mitochondrion.h"

void Mitochondrion::init()
{
	_glocuse_level = 0;
	_has_glocuse_receptor = 0;
}

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	unsigned int i = 0;
	bool NotEmpty = false;
	int correctAminoAcidChain[] = { ALANINE,LEUCINE,GLYCINE,HISTIDINE,LEUCINE,PHENYLALANINE,AMINO_CHAIN_END };

	AminoAcidNode * curr = protein.get_first();

	for (i = 0; (i < 7) && (curr); i++)
	{
		if (curr->get_data() == correctAminoAcidChain[i])
		{
			curr = curr->get_next();
		}
		else
		{
			_has_glocuse_receptor = false;
			return;
		}
		NotEmpty = true;
	}
	//If the list is empty, it skips the for loop, the NotEmpty flag is to prevent to resive a false positive reading.
	if (NotEmpty)
	{
		_has_glocuse_receptor = true;
		return;
	}
}

void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}

bool Mitochondrion::produceATP() const
{
	if (_has_glocuse_receptor && _glocuse_level >= 50)
	{
		return true;
	}
	return false;
}
