#pragma once
#include <iostream>
#include <string>
#include "AminoAcid.h"
#include "Nucleus.h"
#include "Protein.h"

class Ribosome
{
public:
	Protein * create_protein(std::string &RNA_transcript) const;
};