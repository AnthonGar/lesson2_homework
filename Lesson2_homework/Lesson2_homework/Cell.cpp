#include "Cell.h"

void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_nucleus.init(dna_sequence);
	_mitochondrion.init();
	_glocus_receptor_gene = glucose_receptor_gene;
}

bool Cell::get_ATP()
{
	std::string RNA = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein* protein = _ribosome.create_protein(RNA);

	if (protein != nullptr)
	{
		_mitochondrion.insert_glucose_receptor(*protein);
		_mitochondrion.set_glucose(100);
		if (_mitochondrion.produceATP())
		{
			_atp_units = 100;
			return true;
		}
	}
	else
	{
		std::cerr << "nullptr";
		_exit(1);
	}
}