#include "Nucleus.h"

//________________________________________________Gene Part_____________________________________________
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}

unsigned int Gene::get_end() const
{
	return _end;
}

unsigned int Gene::get_start() const
{
	return _start;
}

void Gene::set_on_complementary_dna_strand(bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}

void Gene::set_end(unsigned int end)
{
	_end = end;
}

void Gene::set_start(unsigned int start)
{
	_start = start;
}

//________________________________________________Nucleus Part__________________________________________
void Nucleus::init(const std::string dna_sequence)
{
	//Setting the dna strand
	_DNA_strand = dna_sequence;

	//Setting the complementary dna strand
	_complementary_DNA_strand = "";
	unsigned int i = 0;

	for (i = 0; i < dna_sequence.length(); i++)
	{
		if (dna_sequence[i] == 'G')
		{
			_complementary_DNA_strand += 'C';
		}
		else if (dna_sequence[i] == 'C')
		{
			_complementary_DNA_strand += 'G';
		}
		else if (dna_sequence[i] == 'A')
		{
			_complementary_DNA_strand += 'T';
		}
		else if (dna_sequence[i] == 'T')
		{
			_complementary_DNA_strand += 'A';
		}
		else
		{
			std::cerr << "Invalid Char Found";
			_exit(1);
		}
	}
}

std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	unsigned int i = 0;

	std::string RNA = "";
	std::string strand = "";

	//Which strand we need to use.
	if (gene.is_on_complementary_dna_strand())
	{
		strand = _complementary_DNA_strand;
	}
	else
	{
		strand = _DNA_strand;
	}

	for (i = gene.get_start(); i <= gene.get_end(); i++)
	{
		if (strand[i] != 'T')
		{
			RNA += strand[i];
		}
		else
		{
			RNA += 'U';
		}
	}
	return RNA;
}

std::string Nucleus::get_reversed_DNA_strand() const
{
	std::string reversedStrand = _DNA_strand;
	std::reverse(reversedStrand.begin(), reversedStrand.end());
	return reversedStrand;
}

unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int counter = 0 , i = 0;
	std::string tempStrand = "";

	if (_DNA_strand.length() <= 3)
	{
		if (_DNA_strand == codon)
		{
			counter++;
		}
	}
	else
	{
		for (i = 0; i < _DNA_strand.length() - 3; i++)
		{
			tempStrand = _DNA_strand[i] + _DNA_strand[i + 1] + _DNA_strand[i + 2];
			if (tempStrand == codon)
			{
				counter++;
			}
		}
	}

	return counter;
}