#include "Ribosome.h"

Protein * Ribosome::create_protein(std::string &RNA_transcript) const
{
	std::string RNA = RNA_transcript;
	std::string tempStrand = "";
	Protein* protein = new Protein;

	protein->init();

	while (RNA.length() >= 3)
	{
		tempStrand = RNA.substr(0, 3);//Gives us the first 3 chars.
		if (get_amino_acid(tempStrand) != UNKNOWN)
		{
			protein->add(get_amino_acid(tempStrand));
			RNA = RNA.substr(3, RNA.length() - 3);
		}
		else
		{
			std::cerr << "UNKNOWN - Not an acid";
			delete[] protein;
			return nullptr;
		}
	}
	return protein;
}