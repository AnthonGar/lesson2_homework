#pragma once
#include <string>
#include <iostream>

class Gene
{
private:
	unsigned int _start;//index to the start of the Gene
	unsigned int _end;//index to the end of the Gene

	bool _on_complementary_dna_strand;

public:

	void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);//initialize the Gene.

	bool is_on_complementary_dna_strand() const;
	unsigned int get_end() const;
	unsigned int get_start() const;

	void set_on_complementary_dna_strand(bool on_complementary_dna_strand);
	void set_end(unsigned int end);
	void set_start(unsigned int start);

};


class Nucleus
{
private:
	std::string _DNA_strand;//One strand of the DNA
	std::string _complementary_DNA_strand;//The second strand of the DNA (oppiste of _DNA_strand).

public:
	void init(const std::string dna_sequence);
	std::string get_RNA_transcript(const Gene& gene) const;
	std::string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const std::string& codon) const;

};
